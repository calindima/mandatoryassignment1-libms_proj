
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('pages.urls')),
    path('materials/', include('materials.urls')),
    path('customers/', include('customers.urls')),
    path('inventory/', include('inventory.urls')),
    path('admin/', admin.site.urls),
]
