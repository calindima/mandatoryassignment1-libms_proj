from django.apps import AppConfig


class LibrariansConfig(AppConfig):
    name = 'librarians'
