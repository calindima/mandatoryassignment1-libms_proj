from django.shortcuts import render

from materials.models import InventoryItem

def index(request):
    inventory = InventoryItem.objects.filter(user_id=request.user)
    context = {
        'inventory' : inventory
    }
    return render(request, 'inventory/inventory.html', context)