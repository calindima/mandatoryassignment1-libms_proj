from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='materials'),
    path('<int:material_id>', views.checkout, name='checkout')
]