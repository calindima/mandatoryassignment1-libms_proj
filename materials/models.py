from django.db import models

from django.contrib.auth.models import User


class Material(models.Model):
    materialChoices = [
        ('B', 'Book'),
        ('M', 'Magazine'),
    ]
    title = models.CharField(max_length=200)
    materialType = models.CharField(max_length=1, choices=materialChoices,)
    def __str__(self):
        return self.title

class InventoryItem(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    material_id = models.ForeignKey(Material, on_delete=models.CASCADE)
    timestamp = models.DateField(auto_now_add=True)
    deadline = models.DateField(auto_now_add=False)
    def __str__(self):
        return self.material_id.title