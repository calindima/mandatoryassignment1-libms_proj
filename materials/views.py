from django.shortcuts import render, redirect

from datetime import datetime, timedelta

from .models import Material, InventoryItem

def index(request):
    materials = Material.objects.all()

    context = {
        'materials': materials
    }

    return render(request, 'materials/materials.html', context)

def checkout(request, material_id):
    material = Material.objects.get(pk=material_id)
    if material is not None:
        timestamp = datetime.now()
        if material.materialType == 'B':
            deadline = timestamp + timedelta(days=30)
        else:
            deadline = timestamp + timedelta(days=7)
        inventory_item = InventoryItem.objects.create(user_id=request.user, material_id=material, timestamp=timestamp, deadline=deadline)
        inventory_item.save()
    return redirect('materials')
