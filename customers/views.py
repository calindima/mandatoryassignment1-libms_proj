from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth.models import User

def register(request):
    if request.method == 'POST':
        # get form values
        username = request.POST['username']
        password = request.POST['password']
        password2 = request.POST['password2']
        # check if passwords match
        if password == password2:
            # check username
            if User.objects.filter(username=username).exists():
                return redirect('register')
            else:
                user = User.objects.create_user(username=username, password=password)
                user.save()
                return redirect('login')
    else: 
        return render(request, 'customers/register.html')
    
def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            return redirect('dashboard')
        else:
            return redirect('login')
    else: 
        return render(request, 'customers/login.html')
    
def dashboard(request):
    return render(request, 'customers/dashboard.html')
    
def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        return redirect('index')